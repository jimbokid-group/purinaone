'use strict';
if (!window.console) window.console = {};
if (!window.console.memory) window.console.memory = function() {};
if (!window.console.debug) window.console.debug = function() {};
if (!window.console.error) window.console.error = function() {};
if (!window.console.info) window.console.info = function() {};
if (!window.console.log) window.console.log = function() {};

// sticky footer
//-----------------------------------------------------------------------------
if (!Modernizr.flexbox) {
  (function() {
    var
      $pageWrapper = $('#page-wrapper'),
      $pageBody = $('#page-body'),
      noFlexboxStickyFooter = function() {
        $pageBody.height('auto');
        if ($pageBody.height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
          $pageBody.height($(window).height() - $('#header').outerHeight() - $('#footer').outerHeight());
        } else {
          $pageWrapper.height('auto');
        }
      };
    $(window).on('load resize', noFlexboxStickyFooter);
  })();
}
if (ieDetector.ieVersion == 10 || ieDetector.ieVersion == 11) {
  (function() {
    var
      $pageWrapper = $('#page-wrapper'),
      $pageBody = $('#page-body'),
      ieFlexboxFix = function() {
        if ($pageBody.addClass('flex-none').height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
          $pageWrapper.height($(window).height());
          $pageBody.removeClass('flex-none');
        } else {
          $pageWrapper.height('auto');
        }
      };
    ieFlexboxFix();
    $(window).on('load resize', ieFlexboxFix);
  })();
}


$(function() {

  // placeholder
  //-----------------------------------------------------------------------------
  $('input[placeholder], textarea[placeholder]').placeholder();

  // $('.dropdown-body').niceScroll({
  //   cursorcolor: "#8a2037"
  // });
});

// $(window).on('load resize', function() {
//   if ($(window).outerWidth() < 991) {
//     $('.dropdown-body').getNiceScroll().remove();
//   } else {
//     $('.dropdown-body').niceScroll({
//       cursorcolor: "#8a2037"
//     });
//   }
// })

//MatchHeight Plugn https://github.com/liabru/jquery-match-height

$('.art-preview, .lk-blocks').matchHeight({
  property: 'height'
})

// Pop Up FN
var popup = $('.pop-up'),
  overlay = $('.overlay'),
  duration = 400;

$('.js-close-pop-up , .overlay').on('click', function(e) {
  e.preventDefault();
  closePop();
});

$('body').keydown(function(eventObject) {
  if (eventObject.which === 27) { //Click on ESC btn
    closePop();
  }
});

function closePop() {
  popup.fadeOut(duration);
  overlay.fadeOut(duration);

}

$('.js-show-pop-up').on('click', function(e) {
  e.preventDefault();
  var typePop = $(this).attr('data-atr');
  if (typePop == "repair-password") {

    popup.fadeOut(duration);
  }
  showPop(typePop);
});

function showPop(e) {
  var popUp = $('.' + e);
  if (e == 'main-page-lk' && $(window).outerWidth() < 767) {
    return false;
  } else {

    slickSlider();

  }

  popUp.fadeIn(duration).css({
    'top': $(document).scrollTop() + 100
  });


  overlay.fadeIn(duration);
}

//Register Input-Select Styler ("Form Styler" Plugin)

formStyler();

function formStyler() {
  $('.select-style').styler();
  $('.sort-wrap .chose-msg').styler();
  $('.mail-list input[type=checkbox]').styler();
}

//***************************************************

$('.js-show-dropdown').on('click', function(e) {
  e.preventDefault();
  var parent = $(this).parent();
  if ($(this).hasClass('active')) {
    $(this).removeClass('active').parent().removeClass('open')
    parent.find('.dropdown-body').slideUp('fast');
  } else {
    $(this).addClass('active').parent().addClass('open')
    parent.find('.dropdown-body').slideDown('fast');
  }
});



var menu = $('.menu');
$('.js-show-menu').on('click', function(e) {
  e.preventDefault();
  if ($(this).hasClass('active')) {
    menu.slideUp('fast');
    $(this).removeClass('active');
  } else {
    menu.slideDown('fast');
    $(this).addClass('active');
  }
});

$('.form-group input').on('change', function() {
  $(this).parent().find('.error-msg').addClass('show');
});

//*FancyBox Gallery http://fancyapps.com/fancybox/#license
$(".fancybox").fancybox({
  padding: [54, 39, 69, 39],
  tpl: {
    next: '<a title="Next" class="fancybox-nav fancybox-next next" href="javascript:;"><span class="trigger-arrow"></span><span class="text">следующее фото</span></a>',
    prev: '<a title="Previous" class="fancybox-nav fancybox-prev prev" href="javascript:;"><span class="trigger-arrow"></span><span class="text">предыдущее фото</span></a>'
  }
});

//Slick Slider http://kenwheeler.github.io/slick/

function slickSlider() {
  var slider = $('.slider');
  slider.slick({
    slidesToShow: 1,
    dots: true,
    speed: 300,
  });
  slider.slick("refresh");
}


$('.select-style-search').select2({
  dropdownParent: $('#my_amazing_modal')
});

$('.select-style-search').select2()
  .on("select2:open", function() {
    // $('body').addClass('select-fix');
  })
  .on("select2:close", function() {
    // $('body').removeClass('select-fix');
  })


$('.js-scroll').on('click', function(e) {
  e.preventDefault();
  $('html, body').animate({
    scrollTop: $($.attr(this, 'href')).offset().top
  }, 800);
})



//Replace blocks
if ($('.main-page').length != 0) {
  $(window).on('load resize', function() {
    if ($(window).outerWidth() < 540) {
      replace();
    } else {
      replaceBack();
    }
  });
}

$(window).on('load resize', function() {
  if ($(window).outerWidth() < 540) {
    replace();
  } else {
    replaceBack();
  }
});

function replace() {
  $('.main-page .first').insertAfter('.main-page .club-navigation');
  $('.main-page .second').insertBefore('.main-page .club-navigation');
}

function replaceBack() {
  $('.main-page .first').insertBefore('.main-page .club-navigation');
  $('.main-page .second').insertAfter('.main-page .club-navigation');
}
